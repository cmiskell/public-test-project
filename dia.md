```mermaid
graph LR
  observe(["👀 Observe<br>Monitor the 4 golden signals<br>provided by general metrics<br>(latency, traffic, errors, saturation)<br>for each service, looking for SLA<br>breaches (for latency, errors,<br>saturation) and prioritising for the<br>worst breaches"])
  style observe fill:#fed217,stroke-width:4px,stroke:#dddd
  analysis(["🔬 Analysis<br>Investigate the major<br>causes leading to reduction in availability<br>on GitLab.com.<br><br>What are the reasons for these<br>degradations and outages?<br><br>Investigate to understand the<br>cause."])
  style analysis fill:#fec612
  propose(["💡 Propose Improvements<br>Improvements can be<br>- Changes to the infrastructure<br>- Changes to the application<br>- Changes to our observability<br><br>Each proposed change has an<br>issue in the Scalability tracker, with<br>one or more additional issues in<br>other trackers as required."])
  style propose fill:#fec612
  triage(["🤹 Triage<br>Prioritise backlog according to<br>expected availability<br>improvements<br><br>Tickets can be either delegated to<br>engineering teams, via the<br>infra/dev process, delegated to<br>infrastructure (via TBD process) or<br>implemented by the scalability<br>team"])
  style triage fill:#feaf09
  devanddeploy(["🐿️ Develop & Deploy<br>Prioritise backlog according to<br>expected error budget / availability<br>budget improvements<br><br>Items can either be delegated to<br>engineering teams, via the<br>infra/dev process, delegated to<br>infrastructure (via TBD process) or<br>implemented by the scalability<br>team"])
  style devanddeploy fill:#fea404
  assess(["🔦 Assess<br>Can we see the changes we<br>expected following the deployment<br>of this change?<br><br>If not, why is this?"])
  style assess fill:#fe9900

  observe --> analysis
  analysis --> propose
  
  subgraph Scalability Issue Tracker
     propose --> triage --> devanddeploy --> assess
  end
  assess --> observe
  ```
